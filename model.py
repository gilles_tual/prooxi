from twisted.python.test.test_deprecate import _MockDeprecatedAttribute

__author__ = 'Alexandre Cloquet'

from mongoengine import *

connect('px_boutique', host="176.31.253.73")


class Ville(Document):
    nom = StringField()


class MotClef(Document):
    nom = StringField()


class Page(Document):
    url = StringField()
    ip = StringField()
    motsClefs = ListField(ReferenceField(MotClef))


class Adresse(Document):
    numeroRue = IntField()
    typeVoie = StringField(max_length=50)
    nomRue = StringField
    complementAdresse = StringField()
    ville = ReferenceField(Ville)


class Boutique(Document):
    statut = IntField
    nomAffiche = StringField()
    nomFormate = StringField()
    mail = StringField()
    adresses = ListField(ReferenceField(Adresse))
    pages = ListField(ReferenceField(Page))
