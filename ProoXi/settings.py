# coding=utf-8
# Scrapy settings for ProoXi project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'ProoXi'

SPIDER_MODULES = ['ProoXi.spiders']
NEWSPIDER_MODULE = 'ProoXi.spiders'

DOWNLOADER_MIDDLEWARES = {
    'scrapy.contrib.downloadermiddleware.cookies.CookiesMiddleware': 700,
    'ProoXi.Middleware.RandomUserAgentMiddleware': 400,
    'scrapy.contrib.downloadermiddleware.useragent.UserAgentMiddleware': None,
}

#SPIDER_MIDDLEWARES = {'ProoXi.middlewares.IgnoreVisitedItems': 560}

ITEM_PIPELINES = [
    #'ProoXi.pipelines.ProoxiCheckKeyWordPipeline',
    'ProoXi.pipelines.ProoxiInsertInModel',

]

EXTENSIONS = {'ProoXi.MailSender.StatusMailer': None,
              'scrapy.webservice.WebService': 500}

DATABASE = {'drivername': 'mysql',
            'host': 'localhost',
            'port': '3306',
            'username': 'root',
            'password': 'root',
            'database': 'prooxi8-2'}

LOG_ENABLED = True
LOGSTATS_INTERVAL = 30.0
RETRY_ENABLED = True
RETRY_TIMES = 4
DOWNLOAD_TIMEOUT = 30
CONCURRENT_REQUESTS = 100
COOKIES_ENABLED = True
# COOKIES_DEBUG = True
ROBOTSTXT_OBEY = False
RANDOMIZE_DOWNLOAD_DELAY = True
#CONCURRENT_REQUESTS_PER_IP = 1
#CONCURRENT_REQUESTS_PER_DOMAIN = 1
AUTOTHROTTLE_ENABLED = True
RETRY_HTTP_CODES = [500, 503, 504, 400, 404, 408]
WEBSERVICE_ENABLED = True

COEFF_KEYWORD = 1.5
COEFF_DESC = 4
COEFF_NAME = 3
COEFF_H2 = 0.8
# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'ProoXiBot'

USER_AGENT_LIST = ['ProoXiBot',
                   'BotProoXi',
                   'GoogleBot',
                   'TestBot']

CATEGORY_LIST = ["agence immobilière",
                 "agence de voyage"]

STATUSMAILER_RECIPIENTS = ['jcchauvin@prooxi.fr']
#STATUSMAILER_COMPRESSION = 'gzip'
STATUSMAILER_COMPRESSION = None

MAIL_HOST = 'smtp.gmail.com'
MAIL_PORT = 587
MAIL_USER = 'alexandre.cloquet@gmail.com'
MAIL_PASS = 'rkcoYnd7'
