# coding=utf-8
# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/topics/item-pipeline.html

__author__ = 'Alexandre Cloquet'

from scrapy import log
from KeyWord import KeyWord
from scrapy.exceptions import DropItem
from Database import DatabaseConnexion as DBProXi
from model import MotClef, Page, Boutique
from mongoengine import *
import settings


class ProoxiPipeline(object):
    def process_item(self, ProoXi, spider):
        return ProoXi


class FilterKeywordAndDescriptionPipeline(object):
    def process_item(self, ProoXi, spider):
        if len(str(ProoXi['description'])) <= 5 and len(str(ProoXi['keyword'])) <= 5:
            raise DropItem("Description and keywords are empty")
        elif len(str(ProoXi['description'])) > 5 > len(str(ProoXi['keyword'])):
            raise DropItem("Keywords are empty")
        else:
            return ProoXi


class ProoxiInsertInModel(object):
    def process_item(self, ProoXi, spider):
        title = str(ProoXi['name'])
        boutique = Boutique(statut=1, nomAffiche=unicode(title, "utf8"), nomFormate=self.strtr(title), mail=None, adresse=None, pages=[self.make_page(ProoXi)])
        boutique.save()

    def make_adress(self, ProoXi):
        return None

    def make_page(self, ProoXi):
        url = ProoXi['url']
        ip = str(ProoXi['ip'])
        keyword = self.make_keyword(ProoXi)
        urltmp = Page.objects(url=url).first()
        page = None
        if urltmp is not None:
            if url == urltmp.url:
                urltmp.ip = ip
                urltmp.motsClefs = keyword
                urltmp.save()
                return urltmp
            else:
                page = Page(url=url, ip=ip, motsClefs=keyword).save()
                return page
        else:
            page = Page(url=url, ip=ip, motsClefs=keyword).save()
            return page

    def make_keyword(self, ProoXi):
        list_motclef = []
        print str(type(ProoXi['keyword']))
        keywords = self.cutListKeyword(ProoXi['keyword'])
        for keyword in keywords:
            keyword = keyword.replace("[u", "")
            keyword = keyword.replace("]", "")
            keyword = keyword.replace("[", "")
            keyword = keyword.replace("\n", "")
            keyword = unicode(keyword, "utf8")
            mottmp = MotClef.objects(nom=keyword).first()
            if mottmp is not None:
                if keyword == mottmp.nom:
                    list_motclef.append(mottmp)
                else:
                    tmp = MotClef(nom=keyword).save()
                    list_motclef.append(tmp)
            else:
                tmp = MotClef(nom=keyword).save()
                list_motclef.append(tmp)
        return list_motclef

    def cutListKeyword(self, listKeyWord, char=','):
        return str(listKeyWord).split(char)

    def strtr(self, chaine):
        try:
            if chaine:
                accent = ['é', 'è', 'ê', 'à', 'ù', 'û', 'ç', 'ô', 'î', 'ï', 'â', ' ']
                sans_accent = ['e', 'e', 'e', 'a', 'u', 'u', 'c', 'o', 'i', 'i', 'a', ' ']
                chaine = chaine.replace(" ", "")
                for c, s in zip(accent, sans_accent):
                    chaine = chaine.replace(c, s)
                chaine = unicode(chaine, "utf8")
                return chaine
            else:
                return chaine
        except TypeError:
            return chaine

    def getLemme(self, string):
        if string == '':
            return ' '

        string = self.strtr(string)
        return string


class ProoxiCheckKeyWordPipeline(object):
    def process_item(self, ProoXi, spider):
        tmp = float(0)
        self.percentage = KeyWord.KeyWord(ProoXi['currentCategory'], ProoXi['keyword']).percentagesOfCompatibility()
        tmp += (self.percentage * settings.COEFF_KEYWORD)
        log.msg("Coefficient keyword = " + str(float(tmp)), level=log.DEBUG)

        self.percentage = KeyWord.KeyWord(ProoXi['currentCategory'], ProoXi['name']).percentagesOfCompatibility()
        tmp += (self.percentage * settings.COEFF_NAME)
        log.msg("Coefficient name = " + str(float(tmp)), level=log.DEBUG)

        self.percentage = KeyWord.KeyWord(ProoXi['currentCategory'], ProoXi['description']).percentagesOfCompatibility()
        tmp += (self.percentage * settings.COEFF_DESC)
        log.msg("Coefficient description= " + str(float(tmp)), level=log.DEBUG)

        ProoXi['pourcentage'] = tmp
        if tmp < 10:
            DBProXi.DBProoxi().updatetItem('px_boutique', ProoXi)
            log.msg("Update Item on " + ProoXi['url'], level=log.DEBUG)
            DBProXi.DBProoxi().moveWrongLine(ProoXi)
            log.msg("Move line" + str(ProoXi['pourcentage']), level=log.DEBUG)
        else:
            DBProXi.DBProoxi().updatetItem('px_boutique', ProoXi)
            log.msg("Update Item on " + ProoXi['url'], level=log.DEBUG)
        return ProoXi
