# This package will contain the spiders of your Scrapy project
#
# Please refer to the documentation for information on how to create and manage
# your spiders.

__author__ = 'Alexandre Cloquet'

from scrapy.contrib.spiders import CrawlSpider
from scrapy.selector import HtmlXPathSelector
from Database import DatabaseConnexion as DBProXi
from CheckUrl import CheckUrl as check
from scrapy.http import Request
from ProoXi.items import ProoxiItem
import socket
import time


class ProoXiSpider(CrawlSpider):
    name = 'ProoXi'
    allowed_domains = []
    start_urls = []
    checkurl = check.CheckUrl()
    categoryList = []

    def start_requests(self):
        """


        """
        self.categoryList = self.getCategoryList()
        print len(self.categoryList)
        for current in self.categoryList:
            self.currentCategory = {}
            start_url = DBProXi.DBProoxi().getUrlFromCatgory(current[0])
            for url in start_url:
                self.currentCategory["category"] = current[0]
                self.currentCategory["url"] = url[0]
                yield Request(url[0],
                              callback=lambda r,
                              currentCategory=self.currentCategory: self.parse_(r, currentCategory))

    def parse_(self, response, currentCategory):
        """


        """
        x = HtmlXPathSelector(response)
        ProoXi = ProoxiItem()
        ProoXi['url'] = response.request.headers.get('Referer')
        if not ProoXi['url']:
            ProoXi['url'] = response.url
        ProoXi['name'] = x.select("//title/text()").extract()
        if not ProoXi['name']:
            ProoXi['name'] = x.select('//meta[@property="og:title"]/@content').extract()

        ProoXi['description'] = x.select('//meta[@name="description"]/@content').extract()
        if not ProoXi['description']:
            ProoXi['description'] = x.select('//meta[@property="description"]/@content').extract()
        if not ProoXi['description']:
            ProoXi['description'] = x.select('//meta[@name="Description"]/@content').extract()
        if not ProoXi['description']:
            ProoXi['description'] = x.select('//meta[@name="DESCRIPTION"]/@content').extract()
        if not ProoXi['description']:
            ProoXi['description'] = x.select('//META[@NAME="description"]/@content').extract()
        if not ProoXi['description']:
            ProoXi['description'] = x.select('//META[@NAME="Description"]/@content').extract()
        if not ProoXi['description']:
            ProoXi['description'] = x.select('//META[@NAME="DESCRIPTION"]/@content').extract()

        ProoXi['keyword'] = x.select('//meta[@name="keywords"]/@content').extract()
        if not ProoXi['keyword']:
            ProoXi['keyword'] = x.select('//meta[@property="keywords"]/@content').extract()
        if not ProoXi['keyword']:
            ProoXi['keyword'] = x.select('//meta[@name="Keywords"]/@content').extract()
        if not ProoXi['keyword']:
            ProoXi['keyword'] = x.select('//meta[@name="KEYWORDS"]/@content').extract()
        if not ProoXi['keyword']:
            ProoXi['keyword'] = x.select('//META[@NAME="keywords"]/@content').extract()
        if not ProoXi['keyword']:
            ProoXi['keyword'] = x.select('//META[@NAME="Keywords"]/@content').extract()
        if not ProoXi['keyword']:
            ProoXi['keyword'] = x.select('//META[@NAME="KEYWORDS"]/@content').extract()

        ProoXi['status'] = response.status
        ProoXi['pourcentage'] = 0
        ProoXi['h2'] = x.select('//h2/text()').extract()
        ProoXi['ip'] = 0
        try:
            ProoXi['currentCategory'] = DBProXi.DBProoxi().getCategorieFromUrl(ProoXi['url'])[0]
        except IndexError:
            ProoXi['currentCategory'] = DBProXi.DBProoxi().getCategorieFromUrl(ProoXi['url'])
        #for url in x.select('//a/@href').extract():
        #    if self.checkurl.CheckUrl(response.url, url) and not self.checkurl.isBlackListed(
        #         self.checkurl.getNewUrl(url)):
#               yield Request(self.checkurl.getNewUrl(url), callback=self.parse)
        return ProoXi


    def getCategoryList(self):
        """


        """
        return DBProXi.DBProoxi().getCategories()


SPIDER = ProoXiSpider()
